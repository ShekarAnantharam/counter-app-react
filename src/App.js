import React, { Component } from 'react';

import './App.css';
import Counters from "./components/counters";
import Navbar from "./components/navbar";
class App extends React.Component {
  state={
    counters: [
        {id:1, value:4},
        {id:2, value:0},
        {id:3, value:0},
        {id:4, value:0}
    ]
}
handleDelete = (counterId)=>{
    let {counters}= this.state;
    counters=counters.filter(c=>c.id!==counterId);
    this.setState({counters:counters});    
}
handleIncrement=(counter)=>{
    let {counters}=this.state;
    let index=counters.indexOf(counter);
    counters[index].value+=1;
    this.setState({counters});
}

handleReset=()=>{
    let counters=this.state.counters;
    counters=counters.map(c=>{ c.value=0; return c })
    this.setState({counters:counters})
}
  render() { 
    return <div className="App">
    <Navbar
    totalCounters={this.state.counters.filter(c=>c.value>0).length}/>
    <main className="container">
  <Counters
  counters={this.state.counters}
  onReset={this.handleReset}
  onDelete={this.handleDelete}
  onIncrement={this.handleIncrement}/>

    </main>
    </div>;
  }
}
 
export default App;


